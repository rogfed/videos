import axios from 'axios';

const API_KEY = 'AIzaSyDuc6qdnpC0VJlRoYvoGzYupWyiwB4-A5M';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        maxResults: 5,
        key: API_KEY
    }
});